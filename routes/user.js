var express = require('express');
var router = express.Router();
var userController = require('../controllers/userController');
const { check, validationResult, body } = require('express-validator');
var multer = require('multer');
var path = require('path');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './public/images/img-perfil')
    },
    filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
  })
  var upload = multer({ storage: storage })


  var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './public/images/img-distributor')
    },
    filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
  })
  var uploadDistributor = multer({ storage: storage })

  var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './public/images/img-product')
    },
    filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
  })
  var uploadProduct = multer({ storage: storage })


/* REGISTER. */
router.get('/register', userController.register);
router.post('/register',upload.any(),[
    check('name').isLength({ min: 4 }).withMessage('Nombre necesita un minimo de 4 caracteres'),
    check('last_name').isLength({ min: 4 }).withMessage('Apelido necesita al menos 4 caracteres'),
    check('email').isEmail().withMessage('Colocá un email correcto'),
    check('avatar'),
    check('password').isLength({ min: 5 }).withMessage('Password requiere al menos 5 caracteres')
] , userController.registerProcess);
/* ADMIN HOME. */
router.get('/admin', userController.admin);
/* ADD PRODUCT. */
router.get('/admin/add-product',userController.addProduct);
router.post('/admin/add-product',uploadProduct.any(),userController.addProductProcess);
/* ADD DISTRIBUTOR. */
router.get('/admin/add-distributor',userController.addDistributor);
router.post('/admin/add-distributor',uploadDistributor.any(),userController.addDistributorProcess);
/* LIST PRODUCT & DISTRIBUTOR. */
router.get('/admin/list-distributor',userController.listDistributor);
router.get('/admin/list-products', userController.listProducts);
/* EDIT PRODUCT. */
router.get('/admin/edit-products',userController.editProduct);
router.put('/admin/edit-products',uploadProduct.any(),userController.editProductProcess);




module.exports = router;