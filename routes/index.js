var express = require('express');
var router = express.Router();
var userController = require('../controllers/userController');
const { check, validationResult, body } = require('express-validator');

/* GET home page. */
router.get('/', userController.login);
router.post('/', [
    check('email').isEmail().withMessage('Colocá un email Correcto'),
    check('password').isLength({ min: 4 }).withMessage('Colocá un password')
], userController.loginProcess);
router.get('/logout',userController.logout);


module.exports = router;