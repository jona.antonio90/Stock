# Proyecto Control de stock para productos

Temática: Aplicacion para control de stock de productos, donde podra guardar todo tipo de productos creados por un administrador

Objetivo: El usuario debera sentir comodidad a la hora de subir productos a stock como eliminarlos o dejarlo en suspencion.


# Que uso para la applicacion?

- En la aplicacion hago uso de....

1- Bcrypt para hashear la contraseña
2- Template Engine EJS
3- Express
4- Express-Session
5- Express-Validator
6- File-System
7- Sequelize
8- Sequelize-cli
9- MySql2
10- nodemon
11- path
12- Cookie-Parser


---- La Aplicacion sigue en modo desarrollo ----