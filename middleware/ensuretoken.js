function ensureToken(req,res,next){
    const bearerHeader = req.headers['authorization'];
    next();
}

module.exports = ensureToken;