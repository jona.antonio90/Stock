const db = require('../database/models')
const sequelize = db.sequelize;
const { check, validationResult, body } = require('express-validator');
const bcrypt = require('bcrypt');

const controller = {
    admin: (req, res, next) => {
        res.render('admin', {
            usuarioLogeado: req.session.logged
        })
    },
    login: (req, res, next) => {
        var errors = validationResult(req);
        res.render('index', {
            errors: errors.errors
        })
    },
    loginProcess: (req, res, next) => {
        var errors = validationResult(req);
        db.User.findOne({
            where: {
                email:req.body.email
            }
        })
        .then((user) => {
                if(bcrypt.compareSync(req.body.password,user.password)){
                    req.session.logged = user;
                    res.cookie("UsuarioLogeadoenCookie", user.email,{maxAge:4000});
                }
                
            res.redirect('/user/admin')
        }).catch((err) => {
            return res.render('index', {
                errors: errors.errors
            })
        });

    },
    register: (req, res, next) => {
        var errors = validationResult(req);

        res.render('register', {
            errors: errors
        })
    },
    registerProcess: (req, res, next) => {
        var errors = validationResult(req); 
            if(errors.isEmpty()){
                db.User.create({
                    name: req.body.name,
                    last_name: req.body.last_name,
                    email: req.body.email,
                    avatar:req.files[0].filename,
                    password: bcrypt.hashSync(req.body.password,10)
                })
                res.redirect('/')
            }else{
                res.render('register',{
                    errors: errors.errors
                })
            }
    },
    logout: (req,res,next)=>{
        req.session.destroy()
        res.redirect('/')
    },
    addProduct: (req,res,next)=>{
        db.Distributor.findAll()
        .then((distributors) => {
            res.render('add-product',{
                distributors
            })
        }).catch((err) => {
            console.log(err)
        });
        
    },
    addProductProcess: (req,res,next)=>{
        db.Product.create({
            name: req.body.name,
            stock : req.body.stock,
            distributor_id: req.body.distributor_id,
            color:req.body.color,
            quantity: req.body.quantity,
            description: req.body.description,
            price: req.body.price,
            image: req.files[0].filename
                })
        res.redirect('/user/admin')
    },
    addDistributor: (req,res,next)=>{
       res.render('add-distributor')
    },
    addDistributorProcess: (req,res,next)=>{
        db.Distributor.create({
            name: req.body.name,
            image: req.files[0].filename
        })
        res.redirect('/user/admin')
     },
    listDistributor: (req,res,next)=>{
         db.Distributor.findAll()
         .then((distributors) => {
             res.render('list-distributor',{
                 distributors
             })
         }).catch((err) => {
             console.log(err)
         });
     },
    listProducts: (req,res,next)=>{
        let findDistributors = db.Distributor.findAll()
         let findProducts = db.Product.findAll({
             include:[
                 {association: "distributors"}
             ]
         })

         Promise.all([findDistributors,findProducts])

         .then(([distributors,products]) => {
             res.render('list-products',{distributors,products})
         }).catch((err) => {
             console.log(err)
         });
        
     },
    editProduct: (req,res,next)=>{
         let findProduct = db.Product.findAll({
            include:[{association: "distributors"}]
         })
         let findDistributor = db.Distributor.findAll()
         
         Promise.all([findProduct,findDistributor])

         .then(([products,distributors]) => {
            res.render('edit-product',{products,distributors})

         }).catch((err) => {
             console.log(err)
         });
     },
    editProductProcess:(req,res,next)=>{
        db.Product.update(
            {
            name:req.body.name,
            stock:req.body.stock,
            color:req.body.color,
            quantity:req.body.quantity,
            description:req.body.description,
            price:req.body.price,
            distributor_id:req.body.distributor_id,
            image:req.files[0].filename,
        },
        {
            where:{
                id:req.body.id
            }}
        )
        res.redirect('/user/admin/list-products')
    }
}

module.exports = controller;