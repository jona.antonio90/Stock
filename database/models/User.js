module.exports = function(sequelize,dataTypes){

    cols={
        id:{
            type: dataTypes.INTEGER,
            primaryKey: true
        },
        name:{
            type: dataTypes.TEXT
        },
        last_name:{
            type: dataTypes.TEXT
        },
        avatar:{
            type: dataTypes.TEXT
        }
        ,
        password:{
            type: dataTypes.TEXT
        },
        email:{
            type: dataTypes.TEXT
        }
    },
    config = {
        tableName: 'users',
        timestamps: false
    }

    const User = sequelize.define("User",cols,config);
    return User;

}