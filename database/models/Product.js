const Distributor = require("./Distributor");

module.exports = (sequelize,dataTypes) => {
    const Product = sequelize.define("Product",
    
    {
        id:{
            type: dataTypes.INTEGER,
            primaryKey: true
        },
        name:{
            type: dataTypes.TEXT
        },
        stock:{
            type: dataTypes.INTEGER
        },
        distributor_id:{
            type: dataTypes.INTEGER,
            foreignkey: true
        },
        image:{
            type: dataTypes.TEXT
        },
        color:{
            type:dataTypes.TEXT
        },
        quantity:{
            type:dataTypes.INTEGER
        },
        description:{
            type:dataTypes.TEXT
        },
        price:{
            type: dataTypes.INTEGER
        }

    },
    {
        tableName: "products",
        timestamps: false
    });

    //Asocio la tabla games con la tabla categories para que me traiga los campos de la columna category_id
    //Al ser uno a uno se usa el belongsTo (un juego tiene una categoria)
    Product.associate = function(models) {

        Product.belongsTo(models.Distributor, {
            as: "distributors",
            foreignKey: "distributor_id"
        });

        
        }

    return Product;
}